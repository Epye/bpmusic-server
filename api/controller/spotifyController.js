var requestManager = require("../manager/requestManager")
var _ = require('lodash');
var request = require('request-promise-native');

exports.authorizeUser = function (req, res) {
    console.log("/auth");
    var scopes = ['user-read-private', 'user-read-email', 'playlist-read-private'];
    var state = "user";
    // Create the authorization URL
    var authorizeURL = spotifyApi.createAuthorizeURL(scopes, state);
    res.json(authorizeURL);
}

exports.getPlaylists = function (req, res) {
    console.log('/playlists')
    spotifyApi.setAccessToken(req.headers['authorization']);
    // Get a user's playlists
    var userId = "";

    spotifyApi.getMe()
        .then(function (response) {
            userId = response.body.id;
            return spotifyApi.getUserPlaylists();
        })
        .then(function (data) {
            var response = [];
            data.body.items.forEach(playlist => {
                if (playlist.owner.id == userId) {
                    var image = "http://simpleicon.com/wp-content/uploads/playlist.png";
                    if (playlist.images[0]) {
                        image = playlist.images[0].url
                    }
                    response.push({
                        'id': playlist.id,
                        'name': playlist.name,
                        'picture': image
                    })
                }
            })
            res.json(response);
        });
}

exports.getTracksInPlaylist = function (req, res) {
    var playlistId = req.params.playlistId;
    console.log('/playlists/' + playlistId)

    if (req.headers.authorization) {
        spotifyApi.setAccessToken(req.headers['authorization']);
    }

    var userId = "";

    spotifyApi.getMe()
        .then(function (response) {
            userId = response.body.id;
            return spotifyApi.getPlaylist(userId, playlistId);
        })
        .then(function (response) {
            var total = response.body.tracks.total;
            var promiseArray = [];
            var index = 0;
            var options = {
                'offset': index,
                'fields': 'items(is_local,track(name,id,duration_ms,album(artists)))'
            }
            while (total != 0) {
                promiseArray.push(spotifyApi.getPlaylistTracks(userId, playlistId, options));
                total = total > 100 ? total - 100 : 0;
                index += 100;
                options = {
                    'offset': index
                }
            }

            return Promise.all(promiseArray);
        })
        .then(function (response) {
            var finalResponse = [];
            response.forEach(data => {
                data.body.items.forEach(track => {
                    if (!track.is_local) {
                        var artists = [];
                        track.track.album.artists.forEach(artist => {
                            artists.push(artist.name)
                        })
                        finalResponse.push({
                            'id': track.track.id,
                            'name': track.track.name,
                            'duration': track.track.duration_ms,
                            'artists': _.join(artists, " & ")
                        })
                    }
                })
            });
            res.json(finalResponse);
        })
}

exports.getTracksByBPM = function (req, res) {
    var BPM = req.params.bpm;
    var playlistId = req.params.playlistId;
    console.log('/playlists/' + playlistId + '/' + BPM);
    var listMusic = [];
    spotifyApi.setAccessToken(req.headers['authorization']);

    requestManager.HTTP('127.0.0.1', '/playlists/' + playlistId, 'GET')
        .then(function (response) {
            listMusic = response;
            var ids = [];
            response.forEach(track => {
                ids.push(track.id);
            })
            var arrays = _.chunk(ids, 50);
            var promiseArray = [];
            arrays.forEach(tracks => {
                var urlTracks = tracks.toString();
                while (urlTracks.includes(',')) {
                    urlTracks = urlTracks.replace(',', '%2C')
                }
                promiseArray.push(request({
                    uri: 'https://api.spotify.com/v1/audio-features?ids=' + urlTracks,
                    headers: {
                        'authorization': 'Bearer ' + req.headers['authorization']
                    },
                    json: true
                }));
            })
            return Promise.all(promiseArray);
        })
        .then(function (response) {
            var listBPM = []
            response.forEach(audios => {
                listBPM = _.concat(listBPM, _.filter(audios.audio_features, function (o) {
                    return o.tempo > parseInt(BPM) - 5 && o.tempo < parseInt(BPM) + 50
                }));
            })
            var finalResponse = [];
            listBPM.forEach(bpm => {
                var track = _.find(listMusic, ['id', bpm.id]);
                finalResponse.push({
                    'id': bpm.id,
                    'name': track.name,
                    'bpm': parseInt(bpm.tempo),
                    'duration': track.duration + "",
                    'artists': track.artists
                });
            })
            res.json(finalResponse);
        })
        .catch(function (err) {
            res.sendStatus(500);
        })
}

exports.addCode = function (req, res) {
    console.log('/log');
    var code = req.query.code;
    // Retrieve an access token and a refresh token
    spotifyApi.authorizationCodeGrant(code).then(
        function (data) {
            console.log('The token expires in ' + data.body['expires_in']);
            console.log('The access token is ' + data.body['access_token']);
            console.log('The refresh token is ' + data.body['refresh_token']);

            // Set the access token on the API object to use it in later calls
            spotifyApi.setAccessToken(data.body['access_token']);
            spotifyApi.setRefreshToken(data.body['refresh_token']);
            res.json(data.body['access_token'])
        },
        function (err) {
            console.log('Something went wrong!', err);
        }
    );
}