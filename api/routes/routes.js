'use strict';
var spotifyController = require('../controller/spotifyController')

module.exports = function (app) {
    app.route('/playlists').get(spotifyController.getPlaylists);
    app.route('/playlists/:playlistId').get(spotifyController.getTracksInPlaylist);
    app.route('/playlists/:playlistId/:bpm').get(spotifyController.getTracksByBPM);
    app.route('/auth').get(spotifyController.authorizeUser);
    app.route('/log').get(spotifyController.addCode);
}