var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000
bodyParser = require('body-parser');

var SpotifyWebApi = require('spotify-web-api-node');

// credentials are optional
global.spotifyApi = new SpotifyWebApi({
    clientId: '41451448696d446faa2a9b80b9d44064',
    clientSecret: '1e155829e43249748f73e3ca1236f7a6',
    redirectUri: 'http://localhost:3000/log'
});

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

var routes = require('./api/routes/routes');
routes(app);

app.listen(port);
console.log("Listening on port: " + port)